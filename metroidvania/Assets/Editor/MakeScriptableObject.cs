﻿///
/// @ author GusPassos
/// 

using UnityEngine;
using UnityEditor;

namespace LastMage.Persistance
{
    public class MakeScriptableObject : MonoBehaviour
    {
        [MenuItem("Assets/Create/Database")]
        public static void CreateEventDatabaseAsset()
        {
            EventDatabase database = ScriptableObject.CreateInstance<EventDatabase>();
            AssetDatabase.CreateAsset(database, "Assets/Resources/Database/InGameDatabase/Database.asset");
            AssetDatabase.SaveAssets();
        }
    }
}