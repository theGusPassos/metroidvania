﻿using UnityEngine;

namespace LastMage.Camera
{
    public static class CameraBoundsCalculator
    {
        private static float minX = float.MaxValue;
        private static float maxX = 0;

        private static float minY = float.MaxValue;
        private static float maxY = 0;

        public static  Bounds GetMaxBounds(GameObject g)
        {
            var b = new Bounds(g.transform.position, Vector3.zero);
            foreach (Renderer r in g.GetComponentsInChildren<Renderer>())
            {
                b.Encapsulate(r.bounds);
            }
            return b;
        }
    }
}
