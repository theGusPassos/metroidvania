﻿///
/// @author GusPassos
/// 

using LastMage.ControllerFlow;
using LastMage.Controllers;
using System.Collections.Generic;
using UnityEngine;

namespace LastMage.Camera
{
    /// <summary>
    /// Follows the player respection a focus area
    /// </summary>
    public class CameraFollow : MonoBehaviour
    {
        private static bool instatiated = false;

        public static List<BoxCollider2D>  targets;
        public static List<InputHandler>   players;
        
        private static bool started = false;
        private static int  currentTarget = 0;
        private static bool isChangingChars = false;

        // change in every scene
        private static FocusArea    focusArea;     
        private static Vector2      focusAreaSize;

        private Vector3 futurePosition;
        public float    verticalOffset;
        public float    lookAheadDstX;
        public float    lookSmoothTime;
        public float    verticalSmoothTime;

        private float   currentLookAheadX;
        private float   targetLookAheadX;
        private float   lookAheadDirX;
        private float   smoothLookVelocityX;
        private float   smoothVelocityY;
        private bool    lookAheadStopped = false;

        private CameraLimits cameraLimits;

        private void Awake()
        {
            if(!instatiated)
            {
                targets = new List<BoxCollider2D>();
                players = new List<InputHandler>();

                cameraLimits = GetComponent<CameraLimits>();

                instatiated = true;
            }
            else
            {
                Destroy(gameObject);
            }

            // temporary reload
            if (CharManager.charsInstantiated != null && CharManager.charsInstantiated.Count > 0)
                transform.position = CharManager.charsInstantiated[0].transform.position + new Vector3(0, 0, -10);
        }

        public static void AddTargets(params BoxCollider2D[] colliders)
        {
            targets.AddRange(colliders);

            if(targets.Count == 1)
                focusArea = new FocusArea(targets[0].bounds, focusAreaSize);
        }

        public static void AddPlayers(params InputHandler[] playersToAdd)
        {
            players.AddRange(playersToAdd);
        }

        public static void ChangeSelectedChar(int newSelectedChar)
        {
            currentTarget = newSelectedChar;
            isChangingChars = true;
            focusArea = new FocusArea(targets[currentTarget].bounds, focusAreaSize);
        }

        /// <summary>
        /// Must be called in every new scene
        /// </summary>
        /// <param name="newFocusAreaSize"></param>
        public static void SetNewAreaSize(Vector2 newFocusAreaSize)
        {
            focusAreaSize = newFocusAreaSize;

            if(targets.Count > 0)
                focusArea = new FocusArea(targets[currentTarget].bounds, focusAreaSize);
        }

        public static void StartCamera()
        {
            started = true;
        }

        private void LateUpdate()
        {
            if (started)
            {
                Vector2 focusPosition = focusArea.center + Vector2.up * verticalOffset;

                focusArea.Update(targets[currentTarget].bounds);    

                if (focusArea.velocity.x != 0)
                {
                    lookAheadDirX = Mathf.Sign(focusArea.velocity.x);

                    if (Mathf.Sign(players[currentTarget].GetDirectionalInput().x) == Mathf.Sign(focusArea.velocity.x) &&
                        players[currentTarget].GetDirectionalInput().x != 0)
                    {
                        targetLookAheadX = lookAheadDirX * lookAheadDstX;
                        lookAheadStopped = false;
                    }
                    else
                    {
                        if (!lookAheadStopped)
                        {
                            targetLookAheadX = currentLookAheadX + (lookAheadDirX * lookAheadDstX - currentLookAheadX) / 4f;
                            lookAheadStopped = true;
                        }
                    }
                }

                currentLookAheadX = Mathf.SmoothDamp(currentLookAheadX, targetLookAheadX, ref smoothLookVelocityX, lookSmoothTime);

                focusPosition.y = Mathf.SmoothDamp(transform.position.y, focusPosition.y, ref smoothVelocityY, verticalSmoothTime);
                focusPosition += Vector2.right * currentLookAheadX;
                futurePosition = (Vector3)focusPosition + Vector3.forward * -10;

                if(!CameraLimits.isChanging)
                {
                    if (!isChangingChars)
                    {
                        transform.position = futurePosition;
                    }
                    else
                    {
                        transform.position = new Vector3(Mathf.Lerp(transform.position.x, futurePosition.x, 0.2f),
                                                            Mathf.Lerp(transform.position.y, futurePosition.y, 1f),
                                                            transform.position.z);

                        if (transform.position.x == futurePosition.x ||
                            (transform.position.x < futurePosition.x + 0.1f && transform.position.x > futurePosition.x - 0.1f))
                        {
                            isChangingChars = false;
                        }
                    }
                }

                cameraLimits.CheckCameraOutbounds();
            }
        }



        public void ChangeSelectedCharacter()
        {
            isChangingChars = true;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(1, 0, 0, 0.4f);
            Gizmos.DrawCube(focusArea.center, focusAreaSize);
        }

        private struct FocusArea
        {
            public Vector2 center;
            public Vector2 velocity;
            float left;
            float right;
            float top;
            float bottom;

            public FocusArea(Bounds targetBounds, Vector2 size)
            {
                velocity = Vector2.zero;

                left = targetBounds.center.x - size.x / 2;
                right = targetBounds.center.x + size.x / 2;
                bottom = targetBounds.min.y;
                top = targetBounds.min.y + size.y;

                center = new Vector2((left + right) / 2, (top + bottom) / 2);
            }

            public void Update(Bounds targetBounds)
            {
                float shiftX = 0;
                if (targetBounds.min.x < left)
                {
                    shiftX = targetBounds.min.x - left;
                }
                else if (targetBounds.max.x > right)
                {
                    shiftX = targetBounds.max.x - right;
                }
                left += shiftX;
                right += shiftX;

                float shiftY = 0;
                if (targetBounds.min.y < bottom)
                {
                    shiftY = targetBounds.min.y - bottom;
                }
                else if (targetBounds.max.y > top)
                {
                    shiftY = targetBounds.max.y - top;
                }
                top += shiftY;
                bottom += shiftY;
                center = new Vector2((left + right) / 2, (top + bottom) / 2);
                velocity = new Vector2(shiftX, shiftY);
            }
        }
    }
}
