﻿///
/// @author GusPassos
///

using UnityEngine;

namespace LastMage.Camera
{
    /// <summary>
    /// Sets the camera are from this current scene
    /// </summary>
    public class CameraAreaSetter : MonoBehaviour
    {
        public Vector2 cameraArea;

        private void Start()
        {
            CameraFollow.SetNewAreaSize(cameraArea);
        }
    }
}


