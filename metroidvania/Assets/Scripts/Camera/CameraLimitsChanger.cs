﻿using UnityEngine;

namespace LastMage.Camera
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class CameraLimitsChanger : MonoBehaviour
    {
        private static string lastSession;

        public GameObject sessionParent1;
        public GameObject sessionParent2;

        private void OnTriggerEnter2D(Collider2D coll)
        {
            Bounds sessionBounds;

            if (lastSession == null)
            {
                sessionBounds = CameraBoundsCalculator.GetMaxBounds(sessionParent1);
                lastSession = sessionParent1.name;
            }
            else
            {
                if (lastSession == sessionParent1.name)
                {
                    sessionBounds = CameraBoundsCalculator.GetMaxBounds(sessionParent2);
                    lastSession = sessionParent2.name;
                }
                else if (lastSession == sessionParent2.name)
                {
                    sessionBounds = CameraBoundsCalculator.GetMaxBounds(sessionParent1);
                    lastSession = sessionParent2.name;
                }
                else
                {
                    throw new System.Exception("last session not set");
                }
            }
            
            CameraLimits.SetSceneBounds(sessionBounds.max, sessionBounds.min);
        }
    }
}