﻿///
/// @author GusPassos
///

using LastMage.Camera;
using LastMage.Controllers;
using System.Collections.Generic;
using UnityEngine;

namespace LastMage.ControllerFlow
{
    /// <summary>
    /// Can delete and add new playable characters
    /// </summary>
    public class CharManager : MonoBehaviour
    {
        public GameObject[]                 charsPrefab;
        [HideInInspector]
        public static List<GameObject>      charsInstantiated;
        public static List<CharController>  charControllers;
        
        private void Awake()
        {
            charsInstantiated = new List<GameObject>();
            charControllers = new List<CharController>();
        }

        public static void AddPlayableCharacter(GameObject playableCharacter)
        {
            print("adding a new player");
            charsInstantiated.Add(playableCharacter);
            charControllers.Add(charsInstantiated[charsInstantiated.Count-1].GetComponent<CharController>());

            CameraFollow.AddTargets(charsInstantiated[charsInstantiated.Count-1].GetComponent<BoxCollider2D>());
        }

        public static void RemovePlayableCharacter(string charName)
        {
            
        }
    }
}
