﻿///
/// @author GusPassos
/// 

using LastMage.Camera;
using LastMage.Controllers;
using System.Collections.Generic;
using UnityEngine;

namespace LastMage.ControllerFlow
{
    /// <summary>
    /// Can delete and add new players
    /// </summary>
    public class PlayerManager : MonoBehaviour
    {
        public static int   numberOfPlayers = 0;
        private static bool playerWaitingToPlay = false;

        private static List<InputHandler>   players;
        //private static CharManager          charManager;
        private static CharChanger          charChanger;

        private void Awake()
        {
            players = new List<InputHandler>();

            //charManager = GetComponent<CharManager>();
            charChanger = GetComponent<CharChanger>();
        }

        public void Update()
        {
            if(Input.GetKeyDown(KeyCode.I))
            {
                AddPlayer();
            }
            if(Input.GetKeyDown(KeyCode.R))
            {
                DeletePlayer();
            }
        }

        /// <summary>
        /// Called once in the game initialization
        /// </summary>
        /// <param name="nPlayers"></param>
        public static void InitPlayers(int nPlayers)
        {
            numberOfPlayers = nPlayers;

            for (int i = 0; i < nPlayers; i++)
            {
                if (CharManager.charsInstantiated[i] != null)
                {
                    CreatePlayer(i);
                }
                else
                {
                    playerWaitingToPlay = true;
                    print("One player will wait until a new character appears");
                }
            }

            CameraFollow.AddPlayers(players.ToArray());
            CameraFollow.StartCamera();
        }

        public static void AddPlayer()
        {
            if (numberOfPlayers < 2)
            {
                numberOfPlayers++;
                CreatePlayer(players.Count);
            }
            else
            {
                Debug.LogError("The game has reached the max number of players");
            }
        }

        /// <summary>
        /// Deletes the last player that joined the game
        /// </summary>
        public static void DeletePlayer()
        {
            if (players.Count > 1)
            {
                numberOfPlayers = 1;
                GameObject toDestroy = players[1].gameObject;
                players.Remove(players[1]);
                Destroy(toDestroy);

                numberOfPlayers = 1;
            }
        }
        
        public static void DeleteAllPlayers()
        {
            foreach(InputHandler p in players)
                Destroy(p.gameObject);

            players.Clear();
        }

        private static void CreatePlayer(int id)
        {
            GameObject newPlayer = new GameObject("Player " + id);
            DontDestroyOnLoad(newPlayer);

            newPlayer.AddComponent<InputHandler>();
            InputHandler handler = newPlayer.GetComponent<InputHandler>();
            handler.Init(
                id, 
                CharManager.charsInstantiated[id],
                charChanger
                );

            players.Add(handler);
            CameraFollow.AddPlayers(handler);
        }
    }
}