﻿///
/// @author GusPassos
///

using LastMage.Camera;
using LastMage.Characters;
using LastMage.Controllers;
using UnityEngine;

namespace LastMage.ControllerFlow
{
    /// <summary>
    /// Handles the character change
    /// </summary>
    public class CharChanger : MonoBehaviour
    {
        public void ChangeCharacter(InputHandler handler, int controllingId)
        {
            if(PlayerManager.numberOfPlayers == 1)
            {
                if(CharManager.charsInstantiated.Count > 1)
                {
                    int newControlling = (controllingId==1) ? 0 : 1;

                    CameraFollow.ChangeSelectedChar(newControlling);

                    handler.ChangeCharToControll(
                        CharManager.charsInstantiated[newControlling],
                        newControlling
                        );
                }
                else
                {
                    Debug.LogError("there are no characters to change");
                }
            }
            else
            {
                Debug.LogError("You must wait for the other player accept the request\n" +
                    "Players in game: " + PlayerManager.numberOfPlayers);
            }
        }
    }
}
