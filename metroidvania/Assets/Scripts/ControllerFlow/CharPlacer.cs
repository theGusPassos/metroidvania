﻿///
/// @author GusPassos
/// 

using LastMage.Camera;
using UnityEngine;

namespace LastMage.ControllerFlow
{
    /// <summary>
    /// Character Placer for the game start
    /// </summary>
    public class CharPlacer : MonoBehaviour
    {
        private static bool firstPlacement = true;
        private CharManager playableCharManager;

        private void Awake()
        {
            playableCharManager = GetComponent<CharManager>();
        }

        /// <summary>
        /// Every starter scene has a ScenePCPosition that calls this passing the right position
        /// </summary>
        public void PlaceCharacters(CharacterPosStatus magePos, CharacterPosStatus warriorPos)
        {
            if(firstPlacement)
            {
                InstantiateCharacters(magePos, warriorPos);
            }
            else
            {
                // this doesnt make sense for now
                //if(magePos.transform != null)
                //{
                //    playableCharManager.charsInstantiated[0].transform.position = 
                //        magePos.transform.position;

                //    playableCharManager.charsInstantiated[0].transform.localScale = 
                //        magePos.transform.localScale;
                //}
                
                //if(warriorPos.transform != null)
                //{
                //    playableCharManager.charsInstantiated[1].transform.position =
                //        warriorPos.transform.position;

                //    playableCharManager.charsInstantiated[1].transform.localScale =
                //        warriorPos.transform.localScale;
                //}
            }

            SetCameraTargets();
        }

        private void SetCameraTargets()
        {
            BoxCollider2D[] targets = new BoxCollider2D[CharManager.charsInstantiated.Count];

            for(int i = 0; i < targets.Length; i++)
            {
                targets[i] = CharManager.charsInstantiated[i].GetComponent<BoxCollider2D>();
            }

            CameraFollow.AddTargets(targets);
        }

        private void InstantiateCharacters(CharacterPosStatus magePos, CharacterPosStatus warriorPos)
        {
            if (magePos.transform != null)
            {
                GameObject mage = Instantiate(
                    playableCharManager.charsPrefab[0],
                    magePos.transform.position,
                    magePos.transform.rotation
                    );

                mage.transform.localScale = magePos.transform.localScale;
                mage.name = "Girl";

                CharManager.charsInstantiated.Add(mage);
            }

            if (warriorPos.transform != null)
            {
                GameObject warrior = Instantiate(
                    playableCharManager.charsPrefab[1],
                    warriorPos.transform.position,
                    warriorPos.transform.rotation
                    );

                warrior.transform.localScale = magePos.transform.localScale;
                warrior.name = "Boy";

                CharManager.charsInstantiated.Add(warrior);
            }
        }
    }
}
