﻿///
/// @author GusPassos
///

using LastMage.ControllerFlow;
using UnityEngine;

namespace LastaMage.Events
{
    public class DoorEventManager : MonoBehaviour
    {
        public static int       nextDoor;
        private static float    charDistance = 0.5f;

        public Transform[] doors;

        private void Awake ()
        {
            PlaceCharacterInDoor (nextDoor);
        }

        public void PlaceCharacterInDoor(int door)
        {
            for (int i = 0; i < CharManager.charsInstantiated.Count; i++)
            {
                if (CharManager.charsInstantiated[i] != null)
                {
                    CharManager.charsInstantiated[i].transform.position = doors[door].position + new Vector3(i * charDistance, 0, 0);
                }
            }
        }
    }
}