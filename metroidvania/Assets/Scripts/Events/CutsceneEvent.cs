﻿///
/// @author GusPassos
/// 

using System;
using UnityEngine;

namespace LastMage.Events
{
    public class CutsceneEvent : DialogueEvent
    {
        protected CutsceneStatus current;

        private bool    actTriggered = false;
        private float   timer = 0;
        private float   nextActTimer;
        private Action  nextAction;
        private bool    executeOnEnd = false;

        public override void Execute()
        {
            base.Execute();

            ExecuteNextAct();
        }

        protected virtual void ExecuteNextAct()
        {
            throw new NotImplementedException();
        }

        protected void TriggerAct(Action action, float timeToPerform)
        {
            nextAction = action;
            nextActTimer = timeToPerform;
            actTriggered = true;
        }

        protected void TriggerAct(Action action, float timeToPerform, bool executeOnEnd)
        {
            this.executeOnEnd = executeOnEnd;

            TriggerAct(action, timeToPerform);
        }

        private void Update()
        {
            if (actTriggered)
            {
                timer += Time.deltaTime;

                if (timer > nextActTimer)
                {
                    PerformAct();
                }
            }
        }

        private void PerformAct()
        {
            nextAction();
            ResetTriggeredAction();

            if(executeOnEnd)
            {
                executeOnEnd = false;
                ExecuteNextAct();
            }
        }

        public override void NotifyEndLine(int currentLine)
        {
            current.line = currentLine;
            ExecuteNextAct();
        }

        public void NotifyGoalReached()
        {
            current.goal++;
            ExecuteNextAct();
        }

        private void ResetTriggeredAction()
        {
            timer = 0;
            nextAction = null;
            nextActTimer = 0;
            actTriggered = false;
        }
    }

    public struct CutsceneStatus
    {
        public int act;
        public int goal;
        public int line;
    }
}