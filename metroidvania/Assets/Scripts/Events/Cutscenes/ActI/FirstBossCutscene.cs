﻿///
/// @author GusPassos
/// 

using Characters.Enemies;
using LastMage.ArtificialIntelligence;
using LastMage.ArtificialIntelligence.FiniteStateMachines.PlayableChars;
using LastMage.ControllerFlow;
using UnityEngine;

namespace LastMage.Events.Cutscenes.ActI
{
    public class FirstBossCutscene : CutsceneEvent
    {
        public PathFinder   boyPathFinder;
        public BoyFsm       boyFsm;
        public Transform    boyFirstGoal;

        public SkelletonBoss skelletonBoss;

        protected override void ExecuteNextAct()
        {
            //if (current.act == 0)
            //{
            //    TriggerAct(MakeBoyWalk, 1);
            //}
            //if (current.act == 1)
            //{
            //    TriggerAct(MakeBoyTalk, 1);
            //}
            //if (current.act == 2)
            //{
            //    TriggerAct(MakeBoyFight, 3, true);
            //}
            //if (current.act == 3)
            //{
            //    TriggerAct(MakeBossAttack, 0, true);
            //}
            //if (current.act == 4)
            //{
            //    TriggerAct(MakeBoyReactToAttack, 0.5f);
            //}
            if(current.act == 0)
            {
                TriggerAct(MakeBoyPlayable, 0);
            }
        }

        private void MakeBoyWalk()
        {
            boyPathFinder.SetGoal(boyFirstGoal, this);

            current.act = 1;
        }

        private void MakeBoyTalk()
        {
            dialogueSystem.ShowUntilLine(3);

            current.act = 2;
        }

        private void MakeBoyFight()
        {
            boyFsm.StartIa();

            current.act = 3;
        }

        private void MakeBossAttack()
        {
            boyFsm.StopIa();
            skelletonBoss.DoSomething();

            current.act = 4;
        }

        private void MakeBoyReactToAttack()
        {
            dialogueSystem.ShowNextLine();

            current.act = 5;
        }

        private void MakeBoyPlayable()
        {
            CharManager.AddPlayableCharacter(boyPathFinder.gameObject);

            current.act = 6;
        }

        public override Vector3 GetAnotherCharTalking(int charTalking)
        {
            if(charTalking == 3)
            {
                return boyPathFinder.gameObject.transform.position;
            }
            else
            {
                return Vector2.zero;
            }           
        }
    }
}
