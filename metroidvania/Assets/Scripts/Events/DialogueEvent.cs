﻿///
/// @author GusPassos
///

using LastMage.Persistance;
using LastMage.DialogueSystem;
using System;
using UnityEngine;

namespace LastMage.Events
{
    /// <summary>
    /// Handles the execution of a new dialogue
    /// </summary>
    public class DialogueEvent : UniqueEvent
    {
        public static DialogueManager   dialogueSystem;

        [Tooltip("Cutscenes are managed by the own event")]
        public bool supervisoned = false;

        [Tooltip("when more than one is fulfilled, the one with higher id is used")]
        public DialogueBranch[]         dialogueBranchs;

        [Serializable]
        public struct DialogueBranch
        {
            public string id;
            [Tooltip("Events that must have happened to enter this branch")]
            public string[] requirements;
        }

        public override void Execute ()
        {
            dialogueSystem.LoadSceneLines ( this, supervisoned, ID, GetBranch() );
        }

        public virtual void FinalizeEvent()
        {
            EndEvent();
        }

        /// <summary>
        /// The dialogue manager will notify the end line and asks for instructions
        /// </summary
        public virtual void NotifyEndLine(int currentLine) { }

        public virtual Vector3 GetAnotherCharTalking(int charTalking)
        {
            Debug.LogError("Nenhum outro personagem foi adicionado");
            return Vector2.zero;
        }

        private string GetBranch ()
        {
            for ( int i = dialogueBranchs.Length-1; i >= 0; i-- )
            {
                bool fulfilled = true;

                foreach (string req in dialogueBranchs[i].requirements)
                {
                    if ( !EventDatabase.FindTemporaryEvent( req ) ) fulfilled = false; break;
                }

                if ( fulfilled ) return dialogueBranchs[i].id;
            }
            return "";
        }
    }
}
