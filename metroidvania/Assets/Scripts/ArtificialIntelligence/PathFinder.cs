﻿///
/// @author GusPassos
/// 

using LastMage.Controllers;
using LastMage.Events;
using LastMage.Events.Cutscenes;
using UnityEngine;

namespace LastMage.ArtificialIntelligence
{
    public class PathFinder : MonoBehaviour
    {
        private CharController charController;

        private CutsceneEvent cutsceneEvent;

        private Transform goal;
        private bool hasObjective = false;

        private void Awake()
        {
            charController = GetComponent<CharController>();
        }

        private void Update()
        {
            if (hasObjective)
            {
                FindPathToGoal();
                TestDistanceToGoal();
            }
        }

        private void FindPathToGoal()
        {
            Vector2 directionalInput = Vector2.zero;

            if (goal.position.x < transform.position.x)
            {
                directionalInput.x = -1;
            }
            else
            {
                directionalInput.x = 1;
            }

            charController.SetDirectionalInput(directionalInput);
        }

        private void TestDistanceToGoal()
        {
            if (Vector2.Distance(transform.position, goal.position) <= 2)
            {
                AfterReachGoal();
            }
        }

        private void AfterReachGoal()
        {
            charController.SetDirectionalInput(Vector2.zero);

            hasObjective = false;

            if(cutsceneEvent != null)
            {
                cutsceneEvent.NotifyGoalReached();
                cutsceneEvent = null;
            }
        }

        public void SetGoal(Transform goal)
        {
            this.goal = goal;
            hasObjective = true;
        }

        public void SetGoal(Transform goal, CutsceneEvent cutsceneEvent)
        {
            this.cutsceneEvent = cutsceneEvent;
            SetGoal(goal);
        }
    }
}