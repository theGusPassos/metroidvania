﻿using LastMage.Controllers;
using UnityEngine;

namespace LastMage.ArtificialIntelligence.FiniteStateMachines.PlayableChars
{
    public class BoyFsm : MonoBehaviour
    {
        private CharController controller;
        private bool shouldJump = false;

        private void Awake()
        {
            controller = GetComponent<CharController>();
        }

        private void Update()
        {
            if(shouldJump)
            {
                controller.OnJumpInputDown();
            }
        }

        public void StartIa()
        {
            controller.SetDirectionalInput(Vector2.zero);
            shouldJump = true;
        }

        public void StopIa()
        {
            controller.SetDirectionalInput(Vector2.zero);
            shouldJump = false;
        }
    }
}