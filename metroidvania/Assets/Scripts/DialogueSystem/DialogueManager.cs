﻿///
/// @author GusPassos
///

using LastMage.ControllerFlow;
using LastMage.Events;
using UnityEngine;

namespace LastMage.DialogueSystem
{
    /// <summary>
    /// Instantiate and handles a dialogue box that will show the character line
    /// </summary>
    public class DialogueManager : MonoBehaviour
    {
        private DialogueEvent   fatherEvent;
        private bool            isSupervisoned;
        private DialogueLoader  loadDialogue;

        public GameObject       prefabDialogueBox;
        private GameObject      dialogueBoxCreated;
        private DialogueBox     dialogueBox = null;

        private CharacterLine[] loadedLines;
        private int             currentLine = 0;
        private int             charTalking;
        private int             untilLine = 0;

        private static float    timeToShowSameSpeaker = 0.3f;
        private static float    timeToShowDifferentSpeaker = 0.3f;

        [SerializeField]
        private Vector3         boxHeadDistance = new Vector3(0, 3, 0);

        private void Awake ()
        {
            loadDialogue = new DialogueLoader ();

            DialogueBox.dialogueSystem = this;
            DialogueEvent.dialogueSystem = this;
        }

        /// <summary>
        /// read the file
        /// load every line and character that will say something
        /// start showing the dialogue
        /// </summary>
        /// <param name="sceneID"></param>
        /// <param name="branch"></param>
        public void LoadSceneLines ( DialogueEvent fatherEvent, bool isSupervisoned, string sceneID, string branch  )
        {
            this.fatherEvent = fatherEvent;
            this.isSupervisoned = isSupervisoned;

            loadedLines = loadDialogue.LoadScene ( sceneID, branch );
            ShowFirstLine ();
        }

        /// <summary>
        /// Instantiate the dialogue box and trigger the first line
        /// </summary>
        private void ShowFirstLine ()
        {
            dialogueBoxCreated = Instantiate ( prefabDialogueBox );
            dialogueBox = dialogueBoxCreated.GetComponent<DialogueBox> ();
            currentLine = 0;
            TryShowNextLine ();
        }

        /// <summary>
        /// Is called when the dialog is triggered and by the dialoguebox when 
        /// a line is finished
        /// </summary>
        public void TryShowNextLine ()
        {
            if(!isSupervisoned || untilLine != 0)
            {
                ShowNextLine();
            }
            else
            {
                fatherEvent.NotifyEndLine(currentLine);
            }
        }

        public void ShowUntilLine(int untilLine)
        {
            this.untilLine = untilLine;

            ShowNextLine();
        }

        public void ShowNextLine()
        {
            charTalking = currentLine;
            if (currentLine < loadedLines.Length)
            {
                float timeUntilShow = GetTimeUntilNextLine();

                /// Show in dialog
                dialogueBox.StartShowingText(loadedLines[currentLine].GetLine(), timeUntilShow);

                if (!loadedLines[currentLine].HasMoreLines())
                    currentLine++;

                if(untilLine != 0)
                    untilLine--;
            }
            else
            {
                EndDialogue();
            }
        }

        public float GetTimeUntilNextLine ()
        {
            if (loadedLines[currentLine].timeToHoldUntilShow != 0)
                return loadedLines[currentLine].timeToHoldUntilShow;
            else
                return ( charTalking == currentLine ) ? timeToShowSameSpeaker : timeToShowDifferentSpeaker;
            
        }

        public Vector2 GetBoxPosition ()
        {
            // the current line was actuallized already
            if(loadedLines[charTalking].characterID < 2)
                return CharManager.charsInstantiated[loadedLines[charTalking].characterID].transform.position +
                    boxHeadDistance;
            else
                return fatherEvent.GetAnotherCharTalking(loadedLines[charTalking].characterID) + 
                    boxHeadDistance;

        }

        public void EndDialogue ()
        {
            untilLine = 0;
            fatherEvent.FinalizeEvent();
            Destroy ( dialogueBoxCreated );
        }
    }

    public struct CharacterLine
    {
        public int          characterID;
        public string []    line;
        /// <summary>
        /// The time it takes to show the first line
        /// </summary>
        public float        timeToHoldUntilShow;
        private int         currentLine;

        public CharacterLine ( int characterID, params string[] line )
        {
            this.characterID = characterID;
            this.line = line;
            currentLine = 0;
            timeToHoldUntilShow = 0;
        }

        public CharacterLine ( int characterID, float timeToHoldUntilShow, params string[] line )
        {
            this.characterID = characterID;
            this.line = line;
            this.timeToHoldUntilShow = timeToHoldUntilShow;
            currentLine = 0;
        }

        public string GetLine ()
        {
            string toReturn = null;
            if ( line.Length > 1 )
            {
                toReturn = line[currentLine];
                currentLine++;
                return toReturn;
            }

            return line[0];
        }

        public bool HasMoreLines ()
        {
            return ( line.Length > 1 && currentLine < line.Length );
        }
    }
}

