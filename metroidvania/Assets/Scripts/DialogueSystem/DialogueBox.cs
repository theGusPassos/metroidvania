﻿///
/// @author GusPassos
///

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LastMage.DialogueSystem
{
    public class DialogueBox : MonoBehaviour
    {
        public static DialogueManager   dialogueSystem;
        private Text                    boxText;
        public Font                     fontUsed;

        public RectTransform            textRectTransform;
        public RectTransform            backgroundRectTransform;
        public Image                    backgroundImage;

        [SerializeField]
        [Tooltip("Count how many '|' characers fit in a width=10 text box")]
        private int                     maxNumberInLineRef = 40;
        // proportion between the character size and textbox size
        private float                   proportionDivisor;

        // about showing the the text
        private string[]                textByLine;
        public float                    backgroundFadeSpeed;
        private float                   boxTimer = 0;
        public float                    timeUntilLetter = 0.1f;
        public float                    timeToShowText = 1;
        private float                   timeToWaitUntilText = 0;

        // while showing variables
        private int                     currentLine = 0;
        private int                     currentLetter = 0;
        private bool                    hasReachedEnd = false;

        private enum BoxState
        {
            OUT,
            WAITING_TO_SHOW,
            FADING_IN,
            SHOWING,
            FADING_OUT
        }

        private BoxState boxCurrentState = BoxState.OUT;

        private void Awake ()
        {
            boxText = GetComponentInChildren<Text> ();

            SetProportionSize();
        }

        /// <summary>
        /// Sets the const that will be used to convert font size to textbox size
        /// </summary>
        private void SetProportionSize()
        {
            CharacterInfo info;
            fontUsed.GetCharacterInfo("|"[0], out info);
            float refCharSize = (float)info.advance;

            float lineSize = refCharSize * maxNumberInLineRef;

            // 10 is the text box size when we count how many
            // '|' would fit there
            proportionDivisor = lineSize / 10;
        }

        private void Update ()
        {
            transform.position = dialogueSystem.GetBoxPosition();

            if (Input.GetKeyDown(KeyCode.J))
                JumpText();

            switch ( boxCurrentState )
            {
                case BoxState.WAITING_TO_SHOW:
                    WaitToShowText();
                    break;
                case BoxState.FADING_IN:
                    FadeIn();
                    break;
                case BoxState.SHOWING:
                    ShowTextInDialogueBox();
                    break;
                case BoxState.FADING_OUT:
                    FadeOut();
                    break;
            }
        }

        private void WaitToShowText()
        {
            boxTimer += Time.deltaTime;
            if (boxTimer > timeToWaitUntilText)
            {
                boxTimer = 0;
                boxCurrentState = BoxState.FADING_IN;
            }
        }

        private void FadeIn()
        {
            if (backgroundImage.color.a < 1)
            {
                backgroundImage.color += new Color(0, 0, 0, backgroundFadeSpeed * Time.deltaTime);
            }
            else
            {
                boxTimer = 0;
                boxCurrentState = BoxState.SHOWING;
            }
        }

        private void FadeOut()
        {
            if (backgroundImage.color.a > 0)
            {
                backgroundImage.color -= new Color(0, 0, 0, backgroundFadeSpeed * Time.deltaTime);
            }
            else
            {
                boxTimer = 0;
                boxCurrentState = BoxState.OUT;
                dialogueSystem.TryShowNextLine();
            }
        }

        private void ShowTextInDialogueBox()
        {
            boxTimer += Time.deltaTime;

            // test if there is another line
            if (boxTimer > timeUntilLetter && currentLine < textByLine.Length)
            {
                if (currentLetter < textByLine[currentLine].Length)
                {
                    boxTimer = 0;
                    boxText.text += textByLine[currentLine][currentLetter];
                    currentLetter++;
                }
                else
                {
                    // jumps one line and start from the beginning
                    boxText.text += "\n";
                    currentLine++;
                    hasReachedEnd = true;
                    currentLetter = 0;
                }
            }
            else
            {
                if (boxTimer >= timeToShowText)
                {
                    ResetDialogueBox();
                }
            }
        }

        private void ResetDialogueBox()
        {
            boxTimer = 0;
            currentLine = 0;
            currentLetter = 0;
            boxText.text = "";
            boxCurrentState = BoxState.FADING_OUT;
            hasReachedEnd = false;
        }

        public void JumpText()
        {
            // the text is finished
            //print("current line: " + currentLine.ToString() + " " + (textByLine.Length - 1));
            //print("current letter: " + currentLetter + " " + textByLine[currentLine].Length);

            if (boxCurrentState == BoxState.FADING_OUT || hasReachedEnd )
            {
                print("calling the new one");
                ResetDialogueBox();

                boxCurrentState = BoxState.OUT;
                backgroundImage.color = new Color(0, 0, 0, 0);

                dialogueSystem.TryShowNextLine();
            }
            else if (   boxCurrentState == BoxState.SHOWING || 
                        boxCurrentState == BoxState.WAITING_TO_SHOW ||
                        boxCurrentState == BoxState.FADING_IN)
            {
                backgroundImage.color = new Color(0, 0, 0, 1);

                ShowRemainingText();
            }
        }

        /// <summary>
        /// Shows the entire text if the player chooses to 
        /// </summary>
        private void ShowRemainingText()
        {
            boxText.text = "";
            foreach(string line in textByLine)
                boxText.text += line;

            boxTimer = 0;
            currentLine = textByLine.Length - 1;
            currentLetter = textByLine[currentLine].Length;
            hasReachedEnd = true;

            boxCurrentState = BoxState.SHOWING;
        }

        /// <summary>
        /// Starts showing the text in the dialog box
        /// </summary>
        public void StartShowingText ( string textToShow, float timeToWaitUntilText )
        {
            this.timeToWaitUntilText = timeToWaitUntilText;
            boxText.text = "";

            SetTextByLine(textToShow);
            SetDialogueBoxSize();

            boxCurrentState = BoxState.WAITING_TO_SHOW;
        }

        /// <summary>
        /// Defines what will be written in each line
        /// </summary>
        private void SetTextByLine(string textToShow)
        {
            textByLine = textToShow.TrimEnd().TrimStart().Split("^"[0]);

            for(int i = 0; i < textByLine.Length; i++)
                textByLine[i] = textByLine[i].Replace("^", "").TrimEnd().TrimStart();
        }

        /// <summary>
        /// Sets the dialoguebox's width and height
        /// </summary>
        private void SetDialogueBoxSize ()
        {
            float boxHeight = GetBoxHeight(textByLine.Length);
            float boxWidth = GetBoxWidth(textByLine);

            textRectTransform.sizeDelta = new Vector2 (boxWidth, boxHeight);
            backgroundRectTransform.sizeDelta = new Vector2 (boxWidth + 0.5f, boxHeight);
        }

        /// <summary>
        /// Sets the dialogbox's height based on the number of lines
        /// </summary>
        private float GetBoxHeight(int lines)
        {
            return lines * 1.2f;
        }
        
        /// <summary>
        /// Sets the dialogbox's width based on the longest line
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        private float GetBoxWidth(string[] lines)
        {
            int longestLine = GetLongestLineIndex(lines);
            float width = 0;

            CharacterInfo lineInfo;

            foreach (char c in lines[longestLine])
            {
                fontUsed.GetCharacterInfo(c, out lineInfo);
                width += (float)lineInfo.advance;
            }

            return (width / proportionDivisor);
        }

        private int GetLongestLineIndex(string[] lines)
        {
            int index = 0;
            int longestNumber = 0;

            for(int i = 0; i < lines.Length; i++)
            {
                if(lines[i].Length > longestNumber)
                {
                    longestNumber = lines[i].Length;
                    index = i;
                }
            }

            return index;
        }
    }
}