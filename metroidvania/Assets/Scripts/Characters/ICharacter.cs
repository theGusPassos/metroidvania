﻿///
/// @author GusPassos
///

namespace LastMage.Characters
{
    public interface ICharacter
    {
        void Die();
    }
}