﻿///
/// @author GusPassos
///

using UnityEngine;

namespace LastMage.Characters
{
    public interface IPlayableCharacter : ICharacter
    {
        void SendCommand(KeyCode keyCode);
    }
}
