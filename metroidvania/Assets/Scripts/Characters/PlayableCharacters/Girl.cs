﻿///
/// @author GusPassos
/// 

using LastMage.GameManager;
using UnityEngine;

namespace LastMage.Characters.PlayableCharacters
{
    public class Girl : MonoBehaviour, IPlayableCharacter
    {
        public void SendCommand(KeyCode keyCode)
        {
            throw new System.NotImplementedException();
        }

        public void Die()
        {
            GameOverManager.instance.NotifyDeath();
        }
    }
}
