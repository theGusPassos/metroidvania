﻿using LastMage.Camera;
using LastMage.ControllerFlow;
using UnityEngine;

namespace LastaMage.Utility
{
    /// <summary>
    /// Use this only for debugging
    /// </summary>
    public class InstantiateCharactersHere : MonoBehaviour
    {
        private CharManager playableCharManager;

        public void Start()
        {
            playableCharManager = GameObject.Find("ControllerFlow").GetComponent<CharManager>();

            GameObject mage = Instantiate(
                playableCharManager.charsPrefab[0],
                transform.position,
                transform.rotation
                );

            mage.transform.localScale = transform.localScale;
            mage.name = "Girl";

            CharManager.charsInstantiated.Add(mage);


            GameObject warrior = Instantiate(
                playableCharManager.charsPrefab[1],
                transform.position,
                transform.rotation
                );

            warrior.transform.localScale = transform.localScale;
            warrior.name = "Boy";

            CharManager.charsInstantiated.Add(warrior);

            SetCameraTargets();
            
        }

        private void SetCameraTargets()
        {
            BoxCollider2D[] targets = new BoxCollider2D[CharManager.charsInstantiated.Count];

            for (int i = 0; i < targets.Length; i++)
            {
                targets[i] = CharManager.charsInstantiated[i].GetComponent<BoxCollider2D>();
            }

            CameraFollow.AddTargets(targets);
        }

    }
}

