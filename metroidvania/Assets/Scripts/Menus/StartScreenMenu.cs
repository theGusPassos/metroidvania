﻿///
/// @author GusPassos
///

using LastMage.Controllers;
using UnityEngine;

namespace LastMage.Menus
{
    public class StartScreenMenu : MonoBehaviour
    {
        [HideInInspector]
        public GameStarter gameStarter;

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.S))
            {
                gameStarter.StartGame(1);
            }
            if(Input.GetKeyDown(KeyCode.M))
            {
                gameStarter.StartGame(2);
            }
            if(Input.GetKeyDown(KeyCode.E))
            {

            }
        }
    }
}
