﻿///
/// @author GusPassos
/// 

using LastMage.Battle;
using LastMage.Characters;
using LastMage.ControllerFlow;
using UnityEngine;

namespace LastMage.Controllers
{
    /// <summary>
    /// Handles the player input
    /// </summary>
    public class InputHandler : MonoBehaviour
    {
        public static bool          hasControll = true;
        private int                 playerId;
        private int                 controlling;

        [SerializeField]
        private CharController      charController;
        private IPlayableCharacter  playableCharacter;
        private CharStatus          charStatus;
        private CharChanger         characterChanger;
    
        private Vector2             directionalInput;

        // player's input
        private float keyHorizontal = 0;
        private float joyHorizontal = 0;
        private float keyVertical = 0;
        private float joyVertical = 0;

        public void Init(int playerId, GameObject character, CharChanger characterChanger)
        {
            controlling = playerId;
            this.playerId = playerId;
            this.characterChanger = characterChanger;

            SetCharacterScripts(character);
        }

        public void ChangeCharToControll(GameObject character, int newControlling)
        {
            SetCharacterScripts(character);
            controlling = newControlling;
        }

        private void SetCharacterScripts(GameObject newCharacter)
        {
            charController      = newCharacter.GetComponent<CharController>();
            playableCharacter   = newCharacter.GetComponent<IPlayableCharacter>();
            charStatus          = newCharacter.GetComponent<CharStatus>();
        }

        public static void EnableController(bool hasControll)
        {
            if(!hasControll)
            {
                foreach(CharController charController in CharManager.charControllers)
                    charController.SetDirectionalInput(Vector2.zero);
            }

            InputHandler.hasControll = hasControll;
        }

        public Vector2 GetDirectionalInput()
        {
            return directionalInput;
        }

        private void Update()
        {
            if(hasControll)
            {
                if (playableCharacter == null || charStatus.IsControllable())
                {
                    HandleCharMovement();
                    HandleJump();
                }

                HandleCharChange();
            }
        }

        private void HandleCharMovement()
        {
            keyHorizontal = Input.GetAxisRaw("key_Horizontal_" + playerId);
            keyVertical = Input.GetAxisRaw("key_Vertical_" + playerId);

            if (Input.GetJoystickNames().Length > playerId)
            {
                joyHorizontal = Input.GetAxisRaw("joy_Horizontal_" + playerId);
                joyVertical = Input.GetAxisRaw("joy_Vertical_" + playerId);
            }

            // if the joystick is being used
            if (joyHorizontal == 0 && joyVertical == 0)
            {
                // keyboard input
                directionalInput = new Vector2( keyHorizontal,
                                                keyVertical);
            }
            else
            {
                // joystick input
                directionalInput = new Vector2( joyHorizontal,
                                                joyVertical);
            }

                charController.SetDirectionalInput(directionalInput);
        }

        private void HandleJump()
        {
            if(Input.GetButtonDown("key_Jump_" + playerId) || Input.GetButtonDown("joy_Jump_" + playerId))
            {
                charController.OnJumpInputDown();
            }
            else if (Input.GetButtonUp("key_Jump_" + playerId) || Input.GetButtonUp("joy_Jump_" + playerId))
            {
                charController.OnJumpInputUp();
            }
        }
        
        private void HandleCharChange()
        {
            if (Input.GetButtonDown("key_ChangeChar_" + playerId) || Input.GetButtonDown("joy_ChangeChar_" + playerId))
            {
                characterChanger.ChangeCharacter(this, controlling);
            }
        }
    }
}
