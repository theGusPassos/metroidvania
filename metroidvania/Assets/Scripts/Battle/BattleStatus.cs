﻿///
/// @author GusPassos
/// 

using LastMage.Characters;
using UnityEngine;

namespace LastMage.Battle
{
    public class BattleStatus : MonoBehaviour
    {
        private static GameObject   healthBarPrefab;
        private HealthBar           healthBar;

        public float maxHealthPoints;
        public float currentHealthPoints = 0;

        private ICharacter  character;

        private void Start()
        {
            healthBarPrefab = Resources.Load("UI/HealthBars/BasicEnemyHealthBar") as GameObject;

            character = GetComponent<ICharacter>();

            currentHealthPoints = maxHealthPoints;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.B))
                DealDamage(10);
        }

        public void DealDamage(float atack)
        {
            ShowHealthBar();

            currentHealthPoints -= atack;

            healthBar.SetValueToGo(currentHealthPoints);

            CheckDeath();
        }

        private void CheckDeath()
        {
            if(currentHealthPoints <= 0)
            {
                healthBar.DisableHealthBar();
                character.Die();
            }
        }

        /// <summary>
        /// Instantiates the health bar in the game. Can be used for boss fights, since the 
        /// health bar, in this case, will be shown as soon as the boss appears
        /// </summary>
        public void ShowHealthBar()
        {
            if(healthBar == null)
            {
                healthBar = Instantiate(healthBarPrefab, transform).GetComponent<HealthBar>();
                healthBar.name = "HealthBar";
                healthBar.Initialize(maxHealthPoints);
            }
        }

        public void SetHealthBar(HealthBar healthBar)
        {
            this.healthBar = healthBar;
            this.healthBar.Initialize(maxHealthPoints);
        }
    }
}