﻿///
/// @author GusPassos
/// 

using LastMage.ControllerFlow;
using UnityEngine;

namespace LastMage.Battle
{
    public class HealthBarManager : MonoBehaviour
    {
        private static GameObject girlHealthBarObj;
        private static GameObject boyHealthBarObj;

        public GameObject girlHealthBarPrefab;
        public GameObject boyHealthBarPrefab;

        private HealthBar girlHealthBar;
        private HealthBar boyHealthBar;

        private void Awake()
        { 
            SetHealthBars();

            if (CharManager.charsInstantiated.Count == 1)
            {
                girlHealthBarObj.SetActive(true);
                CharManager.charsInstantiated[0].GetComponent<BattleStatus>().SetHealthBar(girlHealthBar);
            }

            if(CharManager.charsInstantiated.Count > 1)
            {
                boyHealthBarObj.SetActive(true);
                CharManager.charsInstantiated[1].GetComponent<BattleStatus>().SetHealthBar(boyHealthBar);
            }
        }

        private void SetHealthBars()
        {
            girlHealthBarObj    = girlHealthBarPrefab;
            boyHealthBarObj     = boyHealthBarPrefab;

            girlHealthBar       = girlHealthBarObj.GetComponent<HealthBar>();
            boyHealthBar        = boyHealthBarObj.GetComponent<HealthBar>();

            // will be activated if the characters is istantiated
            girlHealthBarObj.SetActive(false);
            boyHealthBarObj.SetActive(false);
        }


        // TODO: instantiate the bar in static members
    }
}