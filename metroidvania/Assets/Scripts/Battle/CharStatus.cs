﻿///
/// @author GusPassos
/// 

using UnityEngine;

namespace LastMage.Battle
{
    public class CharStatus : MonoBehaviour
    {
        private ControllState controllState = ControllState.ALIVE;

        public bool IsControllable()
        {
            if (controllState == ControllState.WITHOUT_CONTROLL ||
                controllState == ControllState.DEAD)
            {
                return false;
            }
            else if (controllState == ControllState.ALIVE)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public enum ControllState
    {
        ALIVE,
        DEAD,
        WITHOUT_CONTROLL
    }
}