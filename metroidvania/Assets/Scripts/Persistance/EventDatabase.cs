﻿///
/// @author GusPassos
/// 

using System.Collections.Generic;
using UnityEngine;

namespace LastMage.Persistance
{
    public class EventDatabase : ScriptableObject
    {
        private static List<string> completedEvents = new List<string>();
        private static List<string> temporaryCompletedEvents = new List<string>();

        public static void StartDatabase ()
        {

        }

        /// <summary>
        /// Reloads the temporary database
        /// </summary>
        public static void ReloadTemporaryDatabase ()
        {
            temporaryCompletedEvents.Clear();
            temporaryCompletedEvents = completedEvents;
        }

        public static bool FindCompletedEvent (string e)
        {
            return completedEvents.Contains ( e );
        }

        public static void SaveCompletedEvent (string e)
        {
            completedEvents.Add ( e );
        }

        public static void SaveTemporaryEvent (string e)
        {
            temporaryCompletedEvents.Add ( e );
        }

        public static bool FindTemporaryEvent (string e)
        {
            return temporaryCompletedEvents.Contains ( e );
        }
    }
}
