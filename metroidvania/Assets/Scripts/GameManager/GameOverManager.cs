﻿///
/// @author GusPassos
///

using LastMage.Controllers;
using LastMage.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LastMage.GameManager
{
    public class GameOverManager : MonoBehaviour
    {
        public static GameOverManager instance;

        public GameObject   gameOverText;
        private GameObject  gameOverInGame;

        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        public void NotifyDeath()
        {
            StartCoroutine(WaitToReload());

            InputHandler.EnableController(false);

            gameOverInGame = Instantiate(gameOverText);

            gameOverInGame.GetComponent<GameOverUI>().ShowText();
        }

        private IEnumerator WaitToReload()
        {
            yield return new WaitForSeconds(2.5f);

            InputHandler.EnableController(true);
            GameObject.Find("SceneLoader").GetComponent<SceneLoader>().LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
