﻿///
/// @author GusPassos
/// 

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LastMage.GameManager
{
    public class SceneLoader : MonoBehaviour
    {
        public static SceneLoader sceneLoader;
        private static bool firstLoad = false;

        private void Awake()
        {
            if(sceneLoader == null)
                sceneLoader = this;
        }

        public void LoadScene(string sceneName)
        {
            StartCoroutine(SwitchScenes(sceneName));
            firstLoad = true;   
        }

        public void LoadScene(int sceneIndex)
        {
            StartCoroutine(SwitchScenes(sceneIndex));
            firstLoad = true;
        }

        private IEnumerator SwitchScenes(string sceneName)
        {
            if (firstLoad && SceneManager.GetActiveScene().buildIndex != 0)
                yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);

            yield return StartCoroutine(LoadSceneAndSetActive(sceneName));
        }

        private IEnumerator SwitchScenes(int sceneIndex)
        {
            if (firstLoad && SceneManager.GetActiveScene().buildIndex != 0)
                yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);

            yield return StartCoroutine(LoadSceneAndSetActive(sceneIndex));
        }

        private IEnumerator LoadSceneAndSetActive(string sceneName)
        {
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            Scene newlyLoadedScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);
            SceneManager.SetActiveScene(newlyLoadedScene);
        }

        private IEnumerator LoadSceneAndSetActive(int sceneIndex)
        {
            yield return SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Additive);
            Scene newlyLoadedScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);
            SceneManager.SetActiveScene(newlyLoadedScene);
        }

        public void LoadFirstScene()
        {
            if (SceneManager.GetActiveScene().buildIndex == 0)
            {
                LoadScene("BeachOfTheChained");
            }
        }
    }
}
