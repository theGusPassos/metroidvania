﻿///
/// @author GusPassos
/// 

using LastMage.ControllerFlow;
using LastMage.Events;
using LastMage.GameManager;
using LastMage.Menus;
using LastMage.Persistance;
using UnityEngine;

namespace LastMage.Controllers
{
    /// <summary>
    /// God object that will set everything
    /// </summary>
    public class GameStarter : MonoBehaviour
    {
        public static bool          gameStarted = false;

        [Tooltip("For fast debug. Doesn't work for multiplayer")]
        public bool                 startScreenEnabled = true;

        private GameProgressLoader  gameProgress;
        public GameObject           startScreen;
        public GameObject           controllerFlow;
        public GameObject           dialogueSystem;
        public GameObject           gameOverManager;
        public GameObject           pcHealthBarCreator;

        private void Awake()
        {
            if(gameStarted)
            {
                Destroy(gameObject);
            }
            else
            {
                gameProgress = new GameProgressLoader();

                PlaceSceneLoader();
                PlaceControllerFlow();
                PlaceDialogueSystem();
                StartGameOverManager();

                if (gameProgress.IsFirstTimePlaying())
                {
                    SceneLoader.sceneLoader.LoadFirstScene();
                }
                else
                {
                    SceneLoader.sceneLoader.LoadScene(gameProgress.GetCurrentScene());
                }
            }
        }

        private void Start()
        {
            // for debugging
            if (startScreenEnabled)
            {
                StartStartScreen();
            }
            else
            {
                StartGame(1);
            }
        }

        private void PlaceSceneLoader()
        {
            GameObject sceneLoader = new GameObject("SceneLoader");
            sceneLoader.AddComponent<SceneLoader>();
        }

        private void PlaceControllerFlow()
        {
            controllerFlow = Instantiate(controllerFlow);
            controllerFlow.name = "ControllerFlow";
        }

        private void PlaceDialogueSystem()
        {
            dialogueSystem = Instantiate(dialogueSystem);
            dialogueSystem.name = "Dialogue System";
        }

        public void StartStartScreen()
        {
            startScreen = Instantiate(startScreen);
            startScreen.name = "StartScreen";
            startScreen.GetComponent<StartScreenMenu>().gameStarter = this;
        }

        public void StartGameOverManager()
        {
            gameOverManager = Instantiate(gameOverManager);
            gameOverManager.name = "GameOverManager";
        }

        /// <summary>
        /// Called to start the game
        /// </summary>
        public void StartGame(int nPlayers)
        {
            InitializePlayers(nPlayers);
            InitializeEvents();
            InitializePcHealthBars();

            gameStarted = true;
        }

        private void InitializePlayers(int nPlayers)
        {
            PlayerManager.InitPlayers(nPlayers);

            if(startScreen.activeInHierarchy)
                Destroy(startScreen);   

            Destroy(gameObject);
        }

        private void InitializeEvents()
        {
            GameObject.Find("LevelInfo").GetComponent<UniqueEventsManager>().SetEvents();
        }
        
        private void InitializePcHealthBars()
        {
            pcHealthBarCreator = Instantiate(pcHealthBarCreator);
            pcHealthBarCreator.name = "PlayableCharactersHealthBarManager";
        }
    }
}
