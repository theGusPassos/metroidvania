# Unity Version Required: 5.5.0f3

download it at: https://unity3d.com/pt/get-unity/download/archive

We shouldn't update our Unity's after the project has began. 
The constant changes in the engine will probably crash our previously implemented mechanics.

If, for some reason, one further update comes with a really interesting feature we may consider updating it.